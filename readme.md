# The problem:

`openssl_verify` does not like the signature.

Until openssl accepts `keccak256` as hashing algorithm, probably never, ***this doesn't work***.

Once wallets support decrypt/encrypt, we could try again with "encrypt message
server-side, decrypt message client-side" kind of thing. Currently 
[it](https://docs.metamask.io/guide/experimental-apis.html#eth-getencryptionpublickey) 
is not yet implemented. It 
[doesn't look like it would use keccak256.](https://github.com/MetaMask/eth-sig-util/blob/master/index.ts#L337)
But, we'll have to see.